﻿using System;
using System.Collections.Generic;
using System.Text;
using ClosedXML.Excel;
using System.Data;

namespace GroupPlanner
{
    public partial class Model
    {
        public void ExportPlanToExcel(string filename, int nbrColumns, List<string> groupInfos)
        {
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add("Groups");
                int row = 1;
                int col = 1;
                int maxNbrRows = 0;

                IXLWorksheet groupWorksheet = wb.Worksheet("Groups");
                for (int i = 0; i < GroupTables.Count; i++)
                {
                    groupWorksheet.Cell(row, col).InsertTable(GroupTables[i]);

                    IXLTable table = wb.Worksheet("Groups").Table(i);
                    table.ShowHeaderRow = false;
                    table.Theme = XLTableTheme.None;
                    table.Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetOutsideBorderColor(XLColor.Gray)
                        .Border.SetInsideBorderColor(XLColor.Gray);
                    table.Column(1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);
                    table.Column(2).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Left);

                    string s = groupInfos[i];
                    if (s.Equals("")) s = "Group " + (i + 1);
                    groupWorksheet.Cell(row, col).Value = s;
                    groupWorksheet.Range(row, col, row, col + 1).Merge().Style
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);
                    groupWorksheet.Column(col+1).Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    groupWorksheet.Range(row, col, row, col + 1).Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                    maxNbrRows = Math.Max(maxNbrRows, GroupTables[i].Rows.Count);
                    col = ((i + 1 ) % nbrColumns) * 3 + 1;
                    if ((i + 1) % nbrColumns == 0)
                    {
                        row += maxNbrRows + 1 + 2;
                        maxNbrRows = 0;
                    }
                }
                groupWorksheet.Columns().AdjustToContents();

                wb.Worksheets.Add(PeopleTable, "Unassigned people");
                IXLWorksheet unassignedPeopleWorksheet = wb.Worksheet("Unassigned people");

                wb.Worksheets.Add(AllPeopleTable, "People list").Columns().AdjustToContents();
                IXLWorksheet allPeopleWorksheet = wb.Worksheet("People list");

                DataTable peopleInfo = new DataTable();
                peopleInfo.Columns.Add("Group index", Type.GetType("System.Int32"));
                peopleInfo.Columns.Add("Actual number people", Type.GetType("System.Int32"));
                peopleInfo.Columns.Add("Expected number people", Type.GetType("System.Int32"));
                for (int i = 0; i < GroupTables.Count; i++)
                {
                    peopleInfo.Rows.Add(i, ActualNbrPeople[i], ExpectedCapacities[i]);
                }
                wb.Worksheets.Add("Other data");
                wb.Worksheet("Other data").Cell(1, 1).Value = "Number groups";
                wb.Worksheet("Other data").Cell(1, 2).Value = GroupTables.Count;
                wb.Worksheet("Other data").Cell(1, 3).Value = "Number columns";
                wb.Worksheet("Other data").Cell(1, 4).Value = nbrColumns;
                wb.Worksheet("Other data").Cell(1, 3).Value = "Number unassigned people";
                wb.Worksheet("Other data").Cell(1, 5).Value = PeopleTable.Rows.Count;
                wb.Worksheet("Other data").Cell(2, 1).InsertTable(peopleInfo);


                wb.SaveAs(filename);
            }
        }

        public void ImportPlanFromExcel(string filename)
        {
            using (XLWorkbook wb = new XLWorkbook(filename))
            {
                ResetGroupTables();
                //nbr prople info
                IXLWorksheet peopleInfoWorksheet = wb.Worksheet("Other data");
                int nbrGroups = int.Parse(peopleInfoWorksheet.Cell(1, 2).Value.ToString());
                int nbrColumns = int.Parse(peopleInfoWorksheet.Cell(1, 4).Value.ToString());
                int nbrUnassignedPeople = int.Parse(peopleInfoWorksheet.Cell(1, 5).Value.ToString());
                int row = 3;
                int col = 1;
                for (int i = 0; i < nbrGroups; i++)
                {
                    int group = int.Parse(peopleInfoWorksheet.Cell(row, col).Value.ToString());
                    int actualNbrPeople = int.Parse(peopleInfoWorksheet.Cell(row, col + 1).Value.ToString());
                    int expectedNbrPeople = int.Parse(peopleInfoWorksheet.Cell(row, col + 2).Value.ToString());
                    ActualNbrPeople.Add(actualNbrPeople);
                    ExpectedCapacities.Add(expectedNbrPeople);
                    row++;
                }


                //groups
                IXLWorksheet groupWorksheet = wb.Worksheet("Groups");
                row = 1;
                col = 1;
                int maxNbrRows = 0;
                int groupIndex = 0;
                for (int i = 0; i < nbrGroups; i++)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Nbr", Type.GetType("System.Int32"));
                    dt.Columns.Add("Name", Type.GetType("System.String"));
                    ReadTable(groupWorksheet, row + 1, col,
                                           filename, ExpectedCapacities[groupIndex], ActualNbrPeople[groupIndex], dt);
                    GroupTables.Add(dt);
                    maxNbrRows = Math.Max(maxNbrRows, dt.Rows.Count);

                    groupIndex++;
                    col += 3;
                    if ((i + 1) % nbrColumns == 0)
                    {
                        col = 1;
                        row += maxNbrRows + 3;
                    }
                }

                //unassigned people
                IXLWorksheet unassignedPeopleWorksheet = wb.Worksheet("Unassigned people");
                PeopleTable.Clear();
                ReadTable(unassignedPeopleWorksheet, 2, 1,
                    filename, nbrUnassignedPeople, nbrUnassignedPeople, PeopleTable);

                //all people list
                /*IXLWorksheet allPeopleWorksheet = wb.Worksheet("People list");
                row = 1;
                col = 1;
                while (!allPeopleWorksheet.Cell(row, col).Value.Equals(""))
                {
                    string name = allPeopleWorksheet.Cell(row, col).Value.ToString();
                    AllPeopleTable.Rows.Add(name);
                    row++;
                }*/

                ReorganizeGroupTables();
                ReorganizePeopleTable();
            }
        }

        private void ReadTable(IXLWorksheet ws, int row, int col, string filename, int expectedNbrPeople, int actualNbrPeople, DataTable dt)
        {
            
            if (expectedNbrPeople == 0) return;
            for (int i = 0; i < actualNbrPeople; i++)
            {
                int nbr = int.Parse(ws.Cell(row, col).Value.ToString());
                string name = ws.Cell(row, col + 1).Value.ToString();
                if (name.Equals("")) throw new Exception("Name cannot be empty: in worksheet "
                        + ws.Name + " cell (" + row + "," + col + ")"
                        + " while reading file " + filename); ;
                dt.Rows.Add(nbr, name);
                AllPeopleTable.Rows.Add(name);
                row++;
            }
            
            for (int i = actualNbrPeople; i < expectedNbrPeople; i++)
            {
                dt.Rows.Add(i + 1, "");
            }
        }
    }
}
