﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace GroupPlanner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.WriteLine(DateTime.Now.ToString(new CultureInfo("en-GB")));
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(UIThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            Application.Run(new MainForm());
        }

        private static void UIThreadException(object sender, ThreadExceptionEventArgs t)
        {
            DialogResult result = DialogResult.Cancel;
            try
            {
                result = ShowThreadExceptionDialog("Unexpected Error Occurred", t.Exception);
                string logText = "============" + Environment.NewLine 
                    + DateTime.Now.ToString(new CultureInfo("en-GB")) 
                    + Environment.NewLine + t.Exception.Message 
                    + Environment.NewLine + t.Exception.StackTrace + Environment.NewLine;
                File.AppendAllText(Path.Combine(Application.StartupPath, "ErrorLog"), logText);
            }
            catch
            {
                try
                {
                    MessageBox.Show("Fatal Windows Forms Error",
                        "Fatal Windows Forms Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop);
                }
                finally
                {
                    Application.Exit();
                }
            }

            // Exits the program when the user clicks Abort.
            //if (result == DialogResult.OK) Application.Exit();
        }

        // Creates the error message and displays it.
        private static DialogResult ShowThreadExceptionDialog(string title, Exception e)
        {
            string msg = e.Message;//+ Environment.NewLine + Environment.NewLine  + "Close Group Planner?";
            return MessageBox.Show(msg, title, MessageBoxButtons.OK,
                MessageBoxIcon.Stop);
        }
    }
}
