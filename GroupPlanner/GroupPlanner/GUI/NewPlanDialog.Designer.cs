﻿namespace GroupPlanner
{
    partial class NewPlanDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNbrGroups = new System.Windows.Forms.Label();
            this.textBoxNbrGroups = new System.Windows.Forms.TextBox();
            this.labelNbrPeople = new System.Windows.Forms.Label();
            this.textBoxNbrPeople = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelNbrGroups
            // 
            this.labelNbrGroups.AutoSize = true;
            this.labelNbrGroups.Location = new System.Drawing.Point(13, 13);
            this.labelNbrGroups.Name = "labelNbrGroups";
            this.labelNbrGroups.Size = new System.Drawing.Size(79, 13);
            this.labelNbrGroups.TabIndex = 0;
            this.labelNbrGroups.Text = "Number groups";
            // 
            // textBoxNbrGroups
            // 
            this.textBoxNbrGroups.Location = new System.Drawing.Point(147, 10);
            this.textBoxNbrGroups.Name = "textBoxNbrGroups";
            this.textBoxNbrGroups.Size = new System.Drawing.Size(84, 20);
            this.textBoxNbrGroups.TabIndex = 1;
            // 
            // labelNbrPeople
            // 
            this.labelNbrPeople.AutoSize = true;
            this.labelNbrPeople.Location = new System.Drawing.Point(12, 42);
            this.labelNbrPeople.Name = "labelNbrPeople";
            this.labelNbrPeople.Size = new System.Drawing.Size(125, 13);
            this.labelNbrPeople.TabIndex = 2;
            this.labelNbrPeople.Text = "Number people (optional)";
            // 
            // textBoxNbrPeople
            // 
            this.textBoxNbrPeople.Location = new System.Drawing.Point(147, 39);
            this.textBoxNbrPeople.Name = "textBoxNbrPeople";
            this.textBoxNbrPeople.Size = new System.Drawing.Size(84, 20);
            this.textBoxNbrPeople.TabIndex = 3;
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(74, 78);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(156, 78);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // NewPlanDialog
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(249, 113);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxNbrPeople);
            this.Controls.Add(this.labelNbrPeople);
            this.Controls.Add(this.textBoxNbrGroups);
            this.Controls.Add(this.labelNbrGroups);
            this.Name = "NewPlanDialog";
            this.Text = "New Plan";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNbrGroups;
        private System.Windows.Forms.TextBox textBoxNbrGroups;
        private System.Windows.Forms.Label labelNbrPeople;
        private System.Windows.Forms.TextBox textBoxNbrPeople;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
    }
}