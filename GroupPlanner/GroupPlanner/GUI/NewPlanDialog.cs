﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GroupPlanner
{
    public partial class NewPlanDialog : Form
    {
        public NewPlanDialog()
        {
            InitializeComponent();
        }

        public string NbrGroups { get { return textBoxNbrGroups.Text; } }
        public string NbrPeople { get { return textBoxNbrPeople.Text; } }
    }
}
