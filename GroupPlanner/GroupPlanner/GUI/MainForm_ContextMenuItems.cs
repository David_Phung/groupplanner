﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace GroupPlanner
{
    public partial class MainForm
    {
        private DataGridView contextMenuSelectedDataGridView;

        private void dataGridViewContextMenu_AddSlot(object sender, EventArgs e)
        {
            model.ChangeExpectedCapacity(dataGridViews.IndexOf(contextMenuSelectedDataGridView), 1);
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridViewContextMenu_RemoveSlot(object sender, EventArgs e)
        {
            model.ChangeExpectedCapacity(dataGridViews.IndexOf(contextMenuSelectedDataGridView), -1);
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridViewContextMenu_InsertGroupBefore(object sender, EventArgs e)
        {
            int groupIndex = dataGridViews.IndexOf(contextMenuSelectedDataGridView);
            model.InsertGroup(groupIndex);
            UpdateTableLayout();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridViewContextMenu_InsertGroupAfter(object sender, EventArgs e)
        {
            int groupIndex = dataGridViews.IndexOf(contextMenuSelectedDataGridView) + 1;
            model.InsertGroup(groupIndex);
            UpdateTableLayout();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridViewContextMenu_RemoveGroup(object sender, EventArgs e)
        {
            int groupIndex = dataGridViews.IndexOf(contextMenuSelectedDataGridView);
            model.RemoveGroup(groupIndex);
            UpdateTableLayout();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridViewContextMenu_MoveBackToPeopleTable(object sender, EventArgs e)
        {
            MoveSelectedPersonBackToPeopleTable(contextMenuSelectedDataGridView);
        }

        private void dataGridViewContextMenu_DeleteCompletely(object sender, EventArgs e)
        {
            MoveSelectedPersonBackToPeopleTable(contextMenuSelectedDataGridView);
            model.DeletePerson(model.PeopleTable.Rows.Count - 1);
            UpdateOverallInfosAfterModelChanged();
        }

        private void MoveSelectedPersonBackToPeopleTable(DataGridView dgv)
        {
            List<CellInfo> fromList = new List<CellInfo>();
            CellInfo from;
            from.GroupIndex = dataGridViews.IndexOf(dgv);
            from.IndexInsideGroup = dgv.SelectedRows[0].Index;
            fromList.Add(from);
            CellInfo to;
            to.GroupIndex = -1; //-1 means PeopleTable
            to.IndexInsideGroup = -1;
            model.MovePeople(fromList, to);
            UpdateInfosAfterModelGroupDataChanged();
        }
    }
}
