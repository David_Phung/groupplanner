﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GroupPlanner
{
    public partial class PeopleInputForm : Form
    {
        private MainForm mainForm;

        public PeopleInputForm(MainForm mainForm)
        {
            this.mainForm = mainForm;
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            mainForm.AddPeople(textBoxInput.Text);
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
