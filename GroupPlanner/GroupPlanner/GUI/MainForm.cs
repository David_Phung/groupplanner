﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GroupPlanner
{
    public partial class MainForm : Form
    {
        private TableLayoutPanel tableLayout;
        private Model model;
        private List<DataGridView> dataGridViews;
        private List<Label> nbrPeopleLabels;
        private List<TextBox> groupInfoTextBoxes;
        private int nbrColumns;
        private Rectangle dragBoxFromMouseDown;
        private string textBeforeChanged;

        public MainForm()
        {
            InitializeComponent();
            this.model = new Model();
            dataGridViews = new List<DataGridView>();
            nbrPeopleLabels = new List<Label>();
            groupInfoTextBoxes = new List<TextBox>();
            textBeforeChanged = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            tableLayout = tableLayoutPanel1;
            textBoxQuickPeopleInput.KeyPress += new KeyPressEventHandler(textbox_OnKeyPressedHandler);
            comboBoxNbrColumns.SelectedIndex = comboBoxNbrColumns.Items.IndexOf("4");

            dataGridViewPeople.CellValueChanged += new DataGridViewCellEventHandler(dataGridViewPeople_CellValueChanged);
            dataGridViewPeople.RowsAdded += new DataGridViewRowsAddedEventHandler(dataGridViewPeople_RowsAdded);
            dataGridViewPeople.LostFocus += new EventHandler(dataGridview_LostFocus);
            dataGridViewPeople.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dataGridView_DataBindingComplete);
            dataGridViewPeople.MouseDown += new MouseEventHandler(dataGridView_MouseDown);
            dataGridViewPeople.MouseMove += new MouseEventHandler(dataGridView_MouseMove);
            dataGridViewPeople.DragOver += new DragEventHandler(dataGridView_DragOver);
            dataGridViewPeople.DragDrop += new DragEventHandler(dataGridView_DragDrop);
            dataGridViewPeople.MouseClick += new MouseEventHandler(dataGridView_MouseClick);
            dataGridViewPeople.UserDeletingRow += new DataGridViewRowCancelEventHandler(dataGridView_UserDeletingRow);
            dataGridViewPeople.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(dataGridViewPeople_EditingControlShowing);

            dataGridViewPeople.DataSource = model.PeopleTable;
            DataGridViewColumnCollection columns = dataGridViewPeople.Columns;
            columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            columns[0].ReadOnly = true;
        }

        public void AddPeople(string inputText)
        {
            string[] lines = inputText.Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            List<string> peopleNames = new List<string>(lines);
            model.AddPeople(peopleNames);
            UpdateOverallInfosAfterModelChanged();
        }

        private void UpdateInfosAfterModelGroupDataChanged()
        {
            for (int i = 0; i < dataGridViews.Count; i++)
            {
                int actualNbrPeople = model.ActualNbrPeople[i];
                int expectedNbrPeople = model.ExpectedCapacities[i];
                if (actualNbrPeople > expectedNbrPeople) nbrPeopleLabels[i].Font = new Font(Label.DefaultFont, FontStyle.Bold);
                else nbrPeopleLabels[i].Font = new Font(Label.DefaultFont, FontStyle.Regular);
                nbrPeopleLabels[i].Text = actualNbrPeople + "/" + expectedNbrPeople + " people";
            }
        }

        private void UpdateOverallInfosAfterModelChanged()
        {
            labelOverAllInfo.Text = "Total: " + model.TotalPeople + " people";
        }

        private void UpdateTableLayout()
        {
            int nbrGroups = model.GroupTables.Count;
            if (nbrGroups == 0) return;

            tableLayout.Controls.Clear();
            dataGridViews.Clear();
            groupInfoTextBoxes.Clear();
            nbrPeopleLabels.Clear();
            tableLayout.ColumnCount = nbrColumns;
            int rowCount = nbrGroups / nbrColumns;
            if (nbrGroups % nbrColumns != 0) rowCount++;
            tableLayout.RowCount = rowCount;
            tableLayout.RowStyles.Clear();
            tableLayout.ColumnStyles.Clear();
            int groupIndex = 0;
            for (int row = 0; row < tableLayout.RowCount; row++)
            {
                tableLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 1f / tableLayout.RowCount));
                for (int col = 0; col < tableLayout.ColumnCount && groupIndex < nbrGroups; col++)
                {
                    tableLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 1f / tableLayout.ColumnCount));
                    CellPanel panel = new CellPanel();
                    tableLayout.Controls.Add(panel);
                    dataGridViews.Add(panel.DataGridView);
                    nbrPeopleLabels.Add(panel.NbrPeopleInfo);
                    groupInfoTextBoxes.Add(panel.GroupInfo);
                    panel.GroupInfo.Text = "Group " + (groupIndex + 1);
                    panel.DataGridView.LostFocus += new EventHandler(dataGridview_LostFocus);
                    panel.DataGridView.DataBindingComplete += new DataGridViewBindingCompleteEventHandler(dataGridView_DataBindingComplete);
                    panel.DataGridView.MouseDown += new MouseEventHandler(dataGridView_MouseDown);
                    panel.DataGridView.MouseMove += new MouseEventHandler(dataGridView_MouseMove);
                    panel.DataGridView.DragOver += new DragEventHandler(dataGridView_DragOver);
                    panel.DataGridView.DragDrop += new DragEventHandler(dataGridView_DragDrop);
                    panel.DataGridView.MouseClick += new MouseEventHandler(dataGridView_MouseClick);
                    panel.DataGridView.DataSource = model.GroupTables[groupIndex];
                    DataGridViewColumnCollection columns = panel.DataGridView.Columns;
                    columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                    columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    panel.DataGridView.UserDeletingRow += new DataGridViewRowCancelEventHandler(dataGridView_UserDeletingRow);
                    groupIndex++;
                }
            }
        }

        private void dataGridView_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            (sender as DataGridView).ClearSelection();
        }

        private void dataGridview_LostFocus(object sender, EventArgs e)
        {
            (sender as DataGridView).ClearSelection();
        }

        private void addPeopleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PeopleInputForm peopleInputForm = new PeopleInputForm(this);
            peopleInputForm.Show();
        }

        private void textbox_OnKeyPressedHandler(object sender, KeyPressEventArgs e)
        {
            TextBox tb = sender as TextBox;
            if (e.KeyChar == (char)Keys.Return)
            {
                List<string> l = new List<string>();
                l.Add(tb.Text);
                model.AddPeople(l);
                tb.Text = "";
                e.Handled = true;
            }
            UpdateOverallInfosAfterModelChanged();
        }

        private void dataGridView_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            e.Cancel = true;
            if (e.Row.Cells[1].Value.ToString().Equals("")) return;
            DataGridView dgv = sender as DataGridView;
            if (dgv == dataGridViewPeople)
            {
                model.DeletePerson(e.Row.Index);
                UpdateOverallInfosAfterModelChanged();
            }else
            {
                MoveSelectedPersonBackToPeopleTable(dgv);
            }
        }

        private void buttonAssign_Click(object sender, EventArgs e)
        {
            model.AssignPeople();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void dataGridView_MouseClick(object sender, MouseEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;
            if (e.Button == MouseButtons.Right)
            {
                ContextMenu m = new ContextMenu();
                dgv.Focus();
                int currentMouseOverRow = dgv.HitTest(e.X, e.Y).RowIndex;

                if (dgv == dataGridViewPeople && currentMouseOverRow >= 0 && !dgv.Rows[currentMouseOverRow].Cells[1].Value.ToString().Equals(""))
                {
                    dgv.Rows[currentMouseOverRow].Selected = true;
                    m.MenuItems.Add("Remove person", new EventHandler(dataGridViewContextMenu_DeleteCompletely));
                }else if (dgv != dataGridViewPeople)
                {
                    m.MenuItems.Add("Add slot", new EventHandler(dataGridViewContextMenu_AddSlot));
                    m.MenuItems.Add("Remove slot", new EventHandler(dataGridViewContextMenu_RemoveSlot));

                    if (currentMouseOverRow >= 0 && !dgv.Rows[currentMouseOverRow].Cells[1].Value.ToString().Equals(""))
                    {
                        dgv.Rows[currentMouseOverRow].Selected = true;
                        m.MenuItems.Add("Unassign person", new EventHandler(dataGridViewContextMenu_MoveBackToPeopleTable));
                        m.MenuItems.Add("Remove person", new EventHandler(dataGridViewContextMenu_DeleteCompletely));
                    }

                    m.MenuItems.Add("Insert group before", new EventHandler(dataGridViewContextMenu_InsertGroupBefore));
                    m.MenuItems.Add("Insert group after", new EventHandler(dataGridViewContextMenu_InsertGroupAfter));
                    m.MenuItems.Add("Remove group", new EventHandler(dataGridViewContextMenu_RemoveGroup));
                }
                contextMenuSelectedDataGridView = dgv;
                m.Show(dgv, new Point(e.X, e.Y));
            }
        }

        private void dataGridView_MouseDown(object sender, MouseEventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            // Get the index of the item the mouse is below.
            int rowIndexFromMouseDown = dataGridView.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.               
                Size dragSize = SystemInformation.DragSize;
                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                                        dragSize);
            }
            else
            {
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
            }
        }

        private void dataGridView_MouseMove(object sender, MouseEventArgs e)
        {
            DataGridView dataGridView = sender as DataGridView;
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty && !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {
                    // Proceed with the drag and drop, passing in the list item.                   
                    DragDropEffects dropEffect = dataGridView.DoDragDrop(dataGridView, DragDropEffects.Move);
                }
            }
        }

        private void dataGridView_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dataGridView_DragDrop(object sender, DragEventArgs e)
        {
            DataGridView targetGridView = sender as DataGridView;
            // The mouse locations are relative to the screen, so they must be
            // converted to client coordinates.
            Point clientPoint = targetGridView.PointToClient(new Point(e.X, e.Y));
            // Get the row index of the item the mouse is below.
            int rowIndexOfItemUnderMouseToDrop = targetGridView.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                List<CellInfo> from = new List<CellInfo>();
                for (int i = 0; i < dataGridViews.Count; i++)
                {
                    DataGridView dgv = dataGridViews[i];
                    for (int j = 0; j < dgv.SelectedRows.Count; j++)
                    {
                        CellInfo cellInfo;
                        cellInfo.GroupIndex = i;
                        cellInfo.IndexInsideGroup = dgv.SelectedRows[j].Index;
                        from.Add(cellInfo);
                    }
                }

                for (int i = 0; i < dataGridViewPeople.SelectedRows.Count; i++)
                {
                    CellInfo cellInfo;
                    cellInfo.GroupIndex = -1; //-1 means PeopleTable
                    cellInfo.IndexInsideGroup = dataGridViewPeople.SelectedRows[i].Index;
                    from.Add(cellInfo);
                }

                CellInfo to;
                if (targetGridView == dataGridViewPeople) to.GroupIndex = -1; //-1 means PeopleTable
                else to.GroupIndex = dataGridViews.IndexOf(targetGridView);
                to.IndexInsideGroup = rowIndexOfItemUnderMouseToDrop;
                model.MovePeople(from, to);
                UpdateInfosAfterModelGroupDataChanged();
            }
        }

        private void dataGridViewPeople_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            int lastDisplayedRowIndex = dataGridViewPeople.FirstDisplayedScrollingRowIndex + dataGridViewPeople.DisplayedRowCount(false) - 1;
            if (lastDisplayedRowIndex < dataGridViewPeople.RowCount - 1)
            {
                dataGridViewPeople.FirstDisplayedScrollingRowIndex = dataGridViewPeople.RowCount - 1;
            }
        }

        private void dataGridViewPeople_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            object o = dataGridViewPeople.Rows[e.RowIndex].Cells["Name"].Value;
            if (o == null || o is DBNull || o.ToString().Trim(new[] { ' ', '\t' }).Equals(""))
            {
                dataGridViewPeople.Rows[e.RowIndex].Cells[1].Value = textBeforeChanged;
            }else
            {
                model.EditPerson(e.RowIndex, textBeforeChanged, (string)o);
            }
        }

        private void dataGridViewPeople_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox tb = (TextBox)e.Control;
            textBeforeChanged = tb.Text;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (NewPlanDialog newPlanDialog = new NewPlanDialog())
            {
                newPlanDialog.StartPosition = FormStartPosition.CenterParent;
                DialogResult result = newPlanDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    int nbrGroups = 0;
                    if (!int.TryParse(newPlanDialog.NbrGroups, out nbrGroups) || nbrGroups <= 0)
                    {
                        using (MyMessageBox messageBox = new MyMessageBox())
                        {
                            messageBox.StartPosition = FormStartPosition.CenterParent;
                            messageBox.Message = "Invalid number groups";
                            messageBox.ShowDialog();
                        }
                        return;
                    }
                    int nbrPeople = 0;
                    if (!int.TryParse(newPlanDialog.NbrPeople, out nbrPeople) || nbrPeople <= 0)
                    {
                        nbrPeople = nbrGroups;
                    }
                    if (!int.TryParse((string)comboBoxNbrColumns.SelectedItem, out nbrColumns))
                    {
                        nbrColumns = 4;
                    }

                    model.ResetGroupTables();
                    model.SetNumberGroups(nbrGroups);
                    model.CalculateInitialCapacities(nbrPeople);

                    UpdateTableLayout();
                    UpdateInfosAfterModelGroupDataChanged();
                    UpdateOverallInfosAfterModelChanged();
                }
            }
        }

        private void comboBoxNbrColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!int.TryParse((string)comboBoxNbrColumns.SelectedItem, out nbrColumns))
            {
                nbrColumns = 4;
            }
            UpdateTableLayout();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void savePeopleTableAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.AddExtension = true;
            saveFileDialog1.DefaultExt = ".xlsx";
            saveFileDialog1.Filter = "Excel file(*.xlsx)|*.xlsx";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                List<string> groupInfos = new List<string>();
                for (int i = 0; i < groupInfoTextBoxes.Count; i++)
                {
                    groupInfos.Add(groupInfoTextBoxes[i].Text);
                }
                model.ExportPlanToExcel(saveFileDialog1.FileName, nbrColumns, groupInfos);
            }
        }

        private void importPlanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                model.ImportPlanFromExcel(openFileDialog1.FileName);
                UpdateTableLayout();
                UpdateInfosAfterModelGroupDataChanged();
                UpdateOverallInfosAfterModelChanged();
            }
        }

        private void buttonUnassignAll_Click(object sender, EventArgs e)
        {
            model.UnassignAll();
            UpdateInfosAfterModelGroupDataChanged();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            model.ClearPeopleTable();
            UpdateOverallInfosAfterModelChanged();
        }
    }
}
