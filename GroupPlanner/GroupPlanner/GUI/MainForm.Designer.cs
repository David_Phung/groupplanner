﻿namespace GroupPlanner
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonAssign = new System.Windows.Forms.Button();
            this.buttonUnassignAll = new System.Windows.Forms.Button();
            this.labelNbrColumns = new System.Windows.Forms.Label();
            this.comboBoxNbrColumns = new System.Windows.Forms.ComboBox();
            this.labelOverAllInfo = new System.Windows.Forms.Label();
            this.dataGridViewPeople = new System.Windows.Forms.DataGridView();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonClear = new System.Windows.Forms.Button();
            this.textBoxQuickPeopleInput = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePeopleTableAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importPlanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addPeopleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeople)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.flowLayoutPanel1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridViewPeople);
            this.splitContainer1.Panel2.Controls.Add(this.flowLayoutPanel2);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxQuickPeopleInput);
            this.splitContainer1.Size = new System.Drawing.Size(809, 432);
            this.splitContainer1.SplitterDistance = 576;
            this.splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 29);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(576, 403);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.buttonAssign);
            this.flowLayoutPanel1.Controls.Add(this.buttonUnassignAll);
            this.flowLayoutPanel1.Controls.Add(this.labelNbrColumns);
            this.flowLayoutPanel1.Controls.Add(this.comboBoxNbrColumns);
            this.flowLayoutPanel1.Controls.Add(this.labelOverAllInfo);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(576, 29);
            this.flowLayoutPanel1.TabIndex = 8;
            // 
            // buttonAssign
            // 
            this.buttonAssign.Location = new System.Drawing.Point(3, 3);
            this.buttonAssign.Name = "buttonAssign";
            this.buttonAssign.Size = new System.Drawing.Size(75, 23);
            this.buttonAssign.TabIndex = 0;
            this.buttonAssign.Text = "Assign";
            this.buttonAssign.UseVisualStyleBackColor = true;
            this.buttonAssign.Click += new System.EventHandler(this.buttonAssign_Click);
            // 
            // buttonUnassignAll
            // 
            this.buttonUnassignAll.AutoSize = true;
            this.buttonUnassignAll.Location = new System.Drawing.Point(84, 3);
            this.buttonUnassignAll.Name = "buttonUnassignAll";
            this.buttonUnassignAll.Size = new System.Drawing.Size(76, 23);
            this.buttonUnassignAll.TabIndex = 10;
            this.buttonUnassignAll.Text = "Unassign All";
            this.buttonUnassignAll.UseVisualStyleBackColor = true;
            this.buttonUnassignAll.Click += new System.EventHandler(this.buttonUnassignAll_Click);
            // 
            // labelNbrColumns
            // 
            this.labelNbrColumns.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNbrColumns.AutoSize = true;
            this.labelNbrColumns.Location = new System.Drawing.Point(166, 8);
            this.labelNbrColumns.Name = "labelNbrColumns";
            this.labelNbrColumns.Size = new System.Drawing.Size(86, 13);
            this.labelNbrColumns.TabIndex = 9;
            this.labelNbrColumns.Text = "Number columns";
            // 
            // comboBoxNbrColumns
            // 
            this.comboBoxNbrColumns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNbrColumns.FormattingEnabled = true;
            this.comboBoxNbrColumns.Items.AddRange(new object[] {
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.comboBoxNbrColumns.Location = new System.Drawing.Point(258, 3);
            this.comboBoxNbrColumns.Name = "comboBoxNbrColumns";
            this.comboBoxNbrColumns.Size = new System.Drawing.Size(64, 21);
            this.comboBoxNbrColumns.TabIndex = 8;
            this.comboBoxNbrColumns.SelectedIndexChanged += new System.EventHandler(this.comboBoxNbrColumns_SelectedIndexChanged);
            // 
            // labelOverAllInfo
            // 
            this.labelOverAllInfo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelOverAllInfo.AutoSize = true;
            this.labelOverAllInfo.Location = new System.Drawing.Point(328, 8);
            this.labelOverAllInfo.Name = "labelOverAllInfo";
            this.labelOverAllInfo.Size = new System.Drawing.Size(78, 13);
            this.labelOverAllInfo.TabIndex = 11;
            this.labelOverAllInfo.Text = "Total: ? people";
            // 
            // dataGridViewPeople
            // 
            this.dataGridViewPeople.AllowDrop = true;
            this.dataGridViewPeople.AllowUserToAddRows = false;
            this.dataGridViewPeople.AllowUserToResizeRows = false;
            this.dataGridViewPeople.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPeople.ColumnHeadersVisible = false;
            this.dataGridViewPeople.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPeople.Location = new System.Drawing.Point(0, 29);
            this.dataGridViewPeople.MultiSelect = false;
            this.dataGridViewPeople.Name = "dataGridViewPeople";
            this.dataGridViewPeople.RowHeadersVisible = false;
            this.dataGridViewPeople.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPeople.Size = new System.Drawing.Size(229, 383);
            this.dataGridViewPeople.TabIndex = 5;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.AutoSize = true;
            this.flowLayoutPanel2.Controls.Add(this.buttonClear);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel2.Size = new System.Drawing.Size(229, 29);
            this.flowLayoutPanel2.TabIndex = 4;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(151, 3);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 1;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // textBoxQuickPeopleInput
            // 
            this.textBoxQuickPeopleInput.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBoxQuickPeopleInput.Location = new System.Drawing.Point(0, 412);
            this.textBoxQuickPeopleInput.Name = "textBoxQuickPeopleInput";
            this.textBoxQuickPeopleInput.Size = new System.Drawing.Size(229, 20);
            this.textBoxQuickPeopleInput.TabIndex = 3;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(809, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.savePeopleTableAsToolStripMenuItem,
            this.importPlanToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.newToolStripMenuItem.Text = "&New Plan";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // savePeopleTableAsToolStripMenuItem
            // 
            this.savePeopleTableAsToolStripMenuItem.Name = "savePeopleTableAsToolStripMenuItem";
            this.savePeopleTableAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.savePeopleTableAsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.savePeopleTableAsToolStripMenuItem.Text = "&Export Plan";
            this.savePeopleTableAsToolStripMenuItem.Click += new System.EventHandler(this.savePeopleTableAsToolStripMenuItem_Click);
            // 
            // importPlanToolStripMenuItem
            // 
            this.importPlanToolStripMenuItem.Name = "importPlanToolStripMenuItem";
            this.importPlanToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.importPlanToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.importPlanToolStripMenuItem.Text = "&Import Plan";
            this.importPlanToolStripMenuItem.Click += new System.EventHandler(this.importPlanToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addPeopleToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "&Edit";
            // 
            // addPeopleToolStripMenuItem
            // 
            this.addPeopleToolStripMenuItem.Name = "addPeopleToolStripMenuItem";
            this.addPeopleToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.addPeopleToolStripMenuItem.Text = "&Add People";
            this.addPeopleToolStripMenuItem.Click += new System.EventHandler(this.addPeopleToolStripMenuItem_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = ".";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "xlsx";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = ".";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 456);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Group Planner";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPeople)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addPeopleToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxQuickPeopleInput;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Button buttonAssign;
        private System.Windows.Forms.DataGridView dataGridViewPeople;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxNbrColumns;
        private System.Windows.Forms.Label labelNbrColumns;
        private System.Windows.Forms.ToolStripMenuItem savePeopleTableAsToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem importPlanToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label labelOverAllInfo;
        private System.Windows.Forms.Button buttonUnassignAll;
        private System.Windows.Forms.Button buttonClear;
    }
}

