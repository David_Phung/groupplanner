﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace GroupPlanner
{
    public struct CellInfo
    {
        /// <summary>
        /// -1 means PeopleTable
        /// </summary>
        public int GroupIndex;
        public int IndexInsideGroup;
    }

    public partial class Model
    {
        public List<DataTable> GroupTables { get; protected set;}
        public List<int> ExpectedCapacities { get; protected set; }
        public List<int> ActualNbrPeople { get; protected set; }
        public DataTable PeopleTable { get; protected set; }

        private int circleCountingGroupIndex;
        private DataTable AllPeopleTable;

        public int TotalPeople { get { return AllPeopleTable.Rows.Count; } }
        public int AssignedPeople { get { return AllPeopleTable.Rows.Count - PeopleTable.Rows.Count; } }

        public Model()
        {
            GroupTables = new List<DataTable>();
            ExpectedCapacities = new List<int>();
            ActualNbrPeople = new List<int>();
            PeopleTable = new DataTable();
            PeopleTable.Columns.Add("Nbr", Type.GetType("System.Int32"));
            PeopleTable.Columns.Add("Name", Type.GetType("System.String"));
            AllPeopleTable = new DataTable();
            AllPeopleTable.Columns.Add("Name", Type.GetType("System.String"));
        }
        
        public void ResetGroupTables()
        {
            foreach (DataTable dt in GroupTables)
            {
                dt.Clear();
            }
            GroupTables.Clear();
            ExpectedCapacities.Clear();
            ActualNbrPeople.Clear();
            AllPeopleTable.Clear();
            circleCountingGroupIndex = 0;
        }

        public void SetNumberGroups(int nbrGroups)
        {
            if (nbrGroups < GroupTables.Count)
            {
                GroupTables.RemoveRange(GroupTables.Count, nbrGroups - GroupTables.Count);
                ExpectedCapacities.RemoveRange(GroupTables.Count, nbrGroups - GroupTables.Count);
                ActualNbrPeople.RemoveRange(GroupTables.Count, nbrGroups - GroupTables.Count);
            }
            else
            {
                for (int i = GroupTables.Count; i < nbrGroups; i++)
                {
                    DataTable table = new DataTable();
                    table.Columns.Add("Nbr", Type.GetType("System.Int32"));
                    table.Columns.Add("Name", Type.GetType("System.String"));
                    GroupTables.Add(table);
                    ExpectedCapacities.Add(0);
                    ActualNbrPeople.Add(0);
                }
            }
        }

        public void CalculateInitialCapacities(int nbrPeople)
        {
            int nbrGroups = GroupTables.Count;
            int[] caps = new int[nbrGroups];
            int n = nbrPeople / nbrGroups;
            for (int i = 0; i < caps.Length; i++)
            {
                caps[i] = n;
            }
            int r = nbrPeople - n * nbrGroups;
            for (int i = 0; i < r; i++)
            {
                caps[i]++;
            }

            for (int i = 0; i < GroupTables.Count; i++)
            {
                ExpectedCapacities[i] = caps[i];
                GroupTables[i].Clear();
                for (int j = 0; j < caps[i]; j++)
                {
                    GroupTables[i].Rows.Add(j + 1, "");
                }
            }
        }

        public void AddPeople(List<string> peopleNames)
        {
            foreach (string name in peopleNames)
            {
                if (name.Trim(new[] { ' ', '\t' }).Equals("")) continue;
                PeopleTable.Rows.Add(PeopleTable.Rows.Count + 1, name);
                AllPeopleTable.Rows.Add(name);
            }
            ReorganizePeopleTable();
        }

        public void EditPerson(int rowIndex, string oldName, string newName)
        {
            PeopleTable.Rows[rowIndex].ItemArray[1] = newName;
            for (int i = 0; i < AllPeopleTable.Rows.Count; i++)
            {
                if (AllPeopleTable.Rows[i][0].Equals(oldName))
                {
                    AllPeopleTable.Rows[i][0] = newName;
                    break;
                }
            }
        }

        public void AssignPeople()
        {
            int nbrGroups = GroupTables.Count;
            for (int i = 0; i < PeopleTable.Rows.Count; i++)
            {
                string name = (string)PeopleTable.Rows[i].ItemArray[1];
                DataTable table = GroupTables[circleCountingGroupIndex];
                int currentIndex = ActualNbrPeople[circleCountingGroupIndex];
                if (currentIndex < table.Rows.Count)
                {
                    table.Rows.RemoveAt(currentIndex);
                    DataRow row = table.NewRow();
                    row[0] = currentIndex + 1;
                    row[1] = name;
                    table.Rows.InsertAt(row, currentIndex);
                    ActualNbrPeople[circleCountingGroupIndex]++;
                }else
                {
                    table.Rows.Add(table.Rows.Count + 1, name);
                    ActualNbrPeople[circleCountingGroupIndex] = table.Rows.Count;
                }
                circleCountingGroupIndex = (circleCountingGroupIndex + 1) % nbrGroups;
            }
            PeopleTable.Rows.Clear();
        }

        public void MovePeople(List<CellInfo> fromList, CellInfo to)
        {
            DataTable toTable = PeopleTable;
            if (to.GroupIndex >= 0) toTable = GroupTables[to.GroupIndex];
            List<DataRow> rows = new List<DataRow>();
            List<DataRow> rowCopies = new List<DataRow>();
            foreach (CellInfo from in fromList)
            {
                DataTable fromTable = PeopleTable;
                if (from.GroupIndex >= 0) fromTable = GroupTables[from.GroupIndex];
                DataRow row = fromTable.Rows[from.IndexInsideGroup];
                DataRow rowCopy = toTable.NewRow();
                rows.Add(row);
                rowCopies.Add(rowCopy);
                rowCopy.ItemArray = row.ItemArray;
            }

            for (int i = 0; i < fromList.Count; i++)
            {
                CellInfo from = fromList[i];
                DataTable fromTable = PeopleTable;
                if (from.GroupIndex >= 0) fromTable = GroupTables[from.GroupIndex];
                fromTable.Rows.Remove(rows[i]);
            }

            for (int i = 0; i < fromList.Count; i++)
            {
                CellInfo from = fromList[i];
                if (to.IndexInsideGroup == -1 ||to.IndexInsideGroup > toTable.Rows.Count)
                {
                    toTable.Rows.Add(rowCopies[i]);
                }
                else
                {
                    toTable.Rows.InsertAt(rowCopies[i], to.IndexInsideGroup);
                }
            }
            ReorganizeGroupTables();
            ReorganizePeopleTable();
        }

        private void ReorganizeGroupTables()
        {
            for (int i = 0; i < GroupTables.Count; i++)
            {
                ActualNbrPeople[i] = 0;
            }

            for (int i = 0; i < GroupTables.Count; i++)
            {
                DataTable dt = GroupTables[i];
                //Remove empty rows
                DataRow[] r = new DataRow[dt.Rows.Count];
                dt.Rows.CopyTo(r, 0);
                for (int j = r.Length - 1; j >= 0; j--)
                {
                    string s = r[j]["Name"].ToString();
                    s = s.Trim(new[] { ' ', '\t' });
                    if (s.Equals("")) dt.Rows.Remove(r[j]);
                    else ActualNbrPeople[i]++;
                }
                //Update nbr column
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dt.Rows[j]["Nbr"] = j + 1;
                }
                //Fill tables to expected capacities
                for (int j = dt.Rows.Count; j < ExpectedCapacities[i]; j++)
                {
                    dt.Rows.Add(j + 1, "");
                }
            }
        }

        private void ReorganizePeopleTable()
        {
            //Remove empty rows
            DataRow[] r = new DataRow[PeopleTable.Rows.Count];
            PeopleTable.Rows.CopyTo(r, 0);
            for (int j = r.Length - 1; j >= 0; j--)
            {
                string s = r[j]["Name"].ToString();
                s = s.Trim(new[] { ' ', '\t' });
                if (s.Equals("")) PeopleTable.Rows.Remove(r[j]);
            }
            //Update nbr column
            for (int j = 0; j < PeopleTable.Rows.Count; j++)
            {
                PeopleTable.Rows[j]["Nbr"] = j + 1;
            }
        }

        public void ChangeExpectedCapacity(int groupIndex, int amount)
        {
            ExpectedCapacities[groupIndex] += amount;
            ExpectedCapacities[groupIndex] = Math.Max(ExpectedCapacities[groupIndex], 0);
            ReorganizeGroupTables();
        }

        public void DeletePerson(int indexInPeopleTable)
        {
            object removed = PeopleTable.Rows[indexInPeopleTable][1];
            PeopleTable.Rows.RemoveAt(indexInPeopleTable);
            ReorganizePeopleTable();

            for (int i = 0; i < AllPeopleTable.Rows.Count; i++)
            {
                if (AllPeopleTable.Rows[i][0].Equals(removed))
                {
                    AllPeopleTable.Rows.RemoveAt(i);
                    break;
                }
            }
        }

        public void UnassignAll()
        {
            for (int i = 0; i < GroupTables.Count; i++)
            {
                DataTable dt = GroupTables[i];
                foreach (DataRow r in dt.Rows)
                {
                    DataRow rowCopy = PeopleTable.NewRow();
                    rowCopy.ItemArray = r.ItemArray;
                    PeopleTable.Rows.Add(rowCopy);
                }
                dt.Clear();
                ActualNbrPeople[i] = 0;
                circleCountingGroupIndex = 0;
            }
            ReorganizeGroupTables();
            ReorganizePeopleTable();
        }

        public void ClearPeopleTable()
        {
            while (PeopleTable.Rows.Count > 0)
            {
                DeletePerson(0);
            }
        }

        public void InsertGroup(int index)
        {
            int t = Math.Min(index ,GroupTables.Count - 1);
            t = GroupTables[t].Rows.Count;

            DataTable dt = new DataTable();
            dt.Columns.Add("Nbr", Type.GetType("System.Int32"));
            dt.Columns.Add("Name", Type.GetType("System.String"));
            for (int i = 0; i < t; i++)
            {
                dt.Rows.Add(i+1, "");
            }
            GroupTables.Insert(index, dt);
            ExpectedCapacities.Insert(index, t);
            ActualNbrPeople.Insert(index, 0);
        }

        public void RemoveGroup(int index)
        {
            List<CellInfo> fromList = new List<CellInfo>();
            for (int i = 0; i < GroupTables[index].Rows.Count; i++)
            {
                CellInfo from;
                from.GroupIndex = index;
                from.IndexInsideGroup = i;
                fromList.Add(from);
            }
            CellInfo to;
            to.GroupIndex = -1;
            to.IndexInsideGroup = -1;
            MovePeople(fromList, to);

            GroupTables.RemoveAt(index);
            ExpectedCapacities.RemoveAt(index);
            ActualNbrPeople.RemoveAt(index);
            circleCountingGroupIndex %= GroupTables.Count;
        }
    }
}
