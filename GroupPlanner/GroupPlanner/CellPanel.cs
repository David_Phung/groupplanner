﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualBasic;

namespace GroupPlanner
{
    class CellPanel : TableLayoutPanel
    {
        public DataGridView DataGridView { get; protected set; }
        public Label NbrPeopleInfo { get; protected set; }
        public TextBox GroupInfo { get; protected set; }

        public CellPanel()
        {
            this.Dock = DockStyle.Fill;
            //Random rnd = new Random();
            //this.BackColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));

            NbrPeopleInfo = new Label();
            NbrPeopleInfo.Text = "?/? people";
            NbrPeopleInfo.Dock = DockStyle.Fill;
            NbrPeopleInfo.AutoSize = true;
            NbrPeopleInfo.Margin = new Padding(0,0,0,2);

            GroupInfo = new TextBox();
            GroupInfo.Dock = DockStyle.Fill;
            GroupInfo.Multiline = true;
            GroupInfo.TextChanged += new EventHandler(groupInfo_TextChanged);
            GroupInfo.BorderStyle = BorderStyle.FixedSingle;
            GroupInfo.TextAlign = HorizontalAlignment.Center;
            GroupInfo.Margin = new Padding(0);

            DataGridView = new DataGridView();
            DataGridView.Dock = DockStyle.Fill;
            DataGridView.AllowDrop = true;

            DataGridView.RowHeadersVisible = false;
            DataGridView.ColumnHeadersVisible = false;
            DataGridView.AllowUserToAddRows = false;
            DataGridView.AllowUserToResizeRows = false;
            DataGridView.ReadOnly = true;
            DataGridView.MultiSelect = false;

            DataGridView.BorderStyle = BorderStyle.None;
            DataGridView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridView.Margin = new Padding(0);

            this.ColumnCount = 1;
            this.RowCount = 3;
            this.ColumnStyles.Clear();
            this.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            this.RowStyles.Clear();
            this.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            this.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            this.RowStyles.Add(new RowStyle(SizeType.AutoSize));

            this.Controls.Add(NbrPeopleInfo);
            this.Controls.Add(GroupInfo);
            this.Controls.Add(DataGridView);
        }



        private void groupInfo_TextChanged(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text.Equals("")) textBox.Height = 20;
            else
            {
                Size sz = new Size(textBox.ClientSize.Width, int.MaxValue);
                TextFormatFlags flags = TextFormatFlags.WordBreak;
                int padding = 5;
                int borders = textBox.Height - textBox.ClientSize.Height + 2;
                sz = TextRenderer.MeasureText(textBox.Text, textBox.Font, sz, flags);
                int h = sz.Height + borders + padding;
                if (textBox.Top + h > this.ClientSize.Height - 10)
                {
                    h = this.ClientSize.Height - 10 - textBox.Top;
                }
                textBox.Height = h;
            }
            textBox.Invalidate();
        }
    }
}
