### Group Planner
A .NET application I developed for a friend. The application allows user to quickly create a number of groups and fill them with member names in just a few clicks. Modifications can be done easily using drag and drop. 
Once done, the plan can be exported to Excel and printed out right away.

![alt text](https://bitbucket.org/David_Phung/groupplanner/raw/f95d5abc5a221a0e92eaee120bd5b1b555076140/Readme/1.png)
![alt text](https://bitbucket.org/David_Phung/groupplanner/raw/f95d5abc5a221a0e92eaee120bd5b1b555076140/Readme/2.png)

More features might be added later.

The target framework is .NET 4.0 or above. Linux computers might be able to run it with Mono framework.
The files needed to run the program are in GroupPlanner.zip.

The project is written in C# and created using Visual Studio 2017, it uses the [ClosedXML library](https://github.com/ClosedXML/ClosedXML)
